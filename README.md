# Proj0-Hello
-------------

## Author
Jeffrey Van Horn

## Contact Address
jeffreyv@uoregon.edu

## Description
A program that prints Hello world and provides practice with version control, turn-in and other mechanisms.

